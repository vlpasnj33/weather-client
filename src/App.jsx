import Search from './components/Search/Search'
import './App.css'

function App() {
  return (
      <div className='container'>
        <Search />
      </div>
  )
}

export default App
