import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSearch } from '@fortawesome/free-solid-svg-icons'
import "../Search/Search.css"
import { useState } from 'react';

const { VITE_SEARCH_CITY, VITE_WEATHER_API_KEY } = import.meta.env;

const Search = () => {
  const [searchTerm, setSearchTerm] = useState('')
  const [cities, setCities] = useState([])

  const handleInputChange = (event) => {
    setSearchTerm(event.target.value);
    if (event.target.value) {
      fetchCities(event.target.value);
    } else {
      setCities([]);
    }
  };

  const fetchCities = async (query) => {
    try {
      const response = await fetch(`${VITE_SEARCH_CITY}?q=${query}&limit=3&appid=${VITE_WEATHER_API_KEY}`);
      if (response.ok) {
        const data = await response.json();
        setCities(data);
      } else {
        console.error('Failed to fetch cities');
      }
    } catch (error) {
      console.error('Error fetching cities:', error);
    }
  };

  return (
    <div className="wrap">
      <form className="search">
        <input 
          type="text" 
          className="searchTerm" 
          placeholder="Назва населеного пункту, країни або регіону" 
          value={searchTerm}
          onChange={handleInputChange} 
        />
        <button 
          type="submit" 
          className="searchButton"
        >
          <FontAwesomeIcon icon={faSearch} />
        </button>
      </form>
      {cities.length > 0 && (
        <ul className="cityList">
          {cities.map(city => (
            <li key={city.lat}>
              {city.local_names?.uk ?? city.name}, {city.state ?? ''}{city.state && city.country && ', '}{city.country ?? ''}
              </li>
          ))}
        </ul>
      )}
    </div>  
  )
}

export default Search